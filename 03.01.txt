So welcome to Chapter 11,
Regular Expressions. Now at this point I really am assuming
that you've mastered the material in the first ten chapters of the book. You know what a loop is, you know what
a dictionary is, you know what a list is. You don't have to write
super complex programs. But I want to be able to use those in
a sentence and not have you say, like, well, could you define that? Well, if I start talking about things
that are hard for you to understand, that's what those previous chapters and
any previous courses are for. So regular expressions are, they're
a very interesting thing in programming. They are
themselves a programming language. They're like this own little
tiny mini programming language, that actually you can use in many
different programming languages. So you can you use them in Python,
you can use them in JavaScript, you can use them in Java. And pretty much any time you have some
kind of a complex string matching or extraction, you will run
across regular expressions. Now another other thing that's really important
to point out is that you don't really need to know regular expressions. As, what I'll be showing you throughout
this lecture is two ways of doing things. One is kind of like the simple,
using simpler capabilities that takes a little longer, and
then using regular expressions, which are, tend to be more succinct
ways but then more complex. So a regular expression itself
is a programming language that is specialized in string matching. It itself is a programming language. And the simple version of it is that it's
just really clever wild card expressions for matching, parsing, and
extracting information from strings, which is a lot of what we've done so
far in this class. And it's just a smart find or a search. Here's the Wikipedia definition
of regular expression. And some of you are going to
like regular expressions. As a matter of fact, some of you might
say, why didn't we start doing regular expressions at the very beginning,
it'll be very natural to you. Because you sort of, some people wanna
think deeply about a problem and come up with this beautiful
expression of the problem, and then go like here computer,
this is my expression. Others like to use simpler multistep. I find them fun. But they are their own language, and it's a language of characters
rather than a language of tokens. So if you think of print, and for,
and if, those are all tokens. They're word-like things, but in regular
expressions, a single character. And it comes from a time in the 70s, and
so it's a very compact little language. Regular expressions are a mark of
coolness for some value of cool. If you know regular expressions,
people are like, whoa, and they're not that hard to know,
but they're so funky and weird that it's kind of
special when you know it. So you should be proud once you
understand regular expressions. It's sort of a mark of awesomeness,
like a little regular expression tattoo like my
little tattoo that I have here. So regular expressions, when you learn it, it's like you've gotten
yourself a really cool tattoo. So here's a quick guide, and
I didn't write this quick guide because, I mean I wrote this quick
guide because I need it. As a matter of fact, I have it with me,
I have it printed out on a piece of paper. And some of the regular
expression stuff I remember and I can write simple regular expressions
without looking at a guide, but other ones,
I need a little bit of help. So if we take a look at sort of this
table, the way this table works is, I told you that regular expressions
is a language of characters. And so instead of if or a tab or something having meaning, what it is,
is a character has meaning. So if you're in a regular expression,
^ means the beginning of a line, $ means the end of the line. It doesn't mean caret or dollar. ^ and $ are special characters. So it's like the programming language,
., *, +, brackets, and parentheses, among other characters, have meaning
in this little programming language. We make a little program that's
really tiny and really short, which you'll see just in a moment. So if you're going to make use
of the regular expressions, you have to import regular expressions. So you have to put that at
the beginning of your application. You have to say import re,
so the re library. And then you make use of
these things with re.search. And so search is the name of
a function that's part of the regular expression library. You pass some parameters, and it's kind of
like using the find method on strings. And there's a couple of different
ways that you can take things. You can either use it to search for
things and tell you, did you find something that matched
this regular expression in the string? Or look through the string and pull out
pieces that match the regular expression. Okay, so here's our first example. And this is an example that tells us, very
much like some of our earlier examples, we are looking through like
the needle in the haystack problem. where we're going to open a file, and
we're going to loop through that file. And we're going to take away the suffix,
the blanks at the end, and we're going to check to see if the line,
and we're using the find to say, does this line have
the string 'From:' in it? And so we're saying, what position is it? And the position is line.find, and
if that position is >= 0, away we go. So then we can print the line. So this is like, what this code really
does is it looks through a whole file and shows you the lines that start with "from",
that's what this problem is solving. Okay? By now that should
be relatively simple. Well, if we do the exact same thing in
regular expressions. First we have to import the regular expression library,
let me change my color here. Import the regular expression library. Some of this is the same. We open the file, we loop through the
file, we strip the characters at the end. And so now we're going to use,
instead of the find operation, we're going to use re.search. So re.search is going to return us
a true or a false, and it has an regular expression. Now later we will see that
this can be very complex. None of those characters F-r-o-m-colon are
special regular expression characters, and this is the thing we're looking at. We're saying find,
look through this line here, and find that string 'From:'. If it's there, you
get a true, if it's not, you get a false. So this also will loop through the file, printing out the lines that have
the string 'From:' in them somewhere. So we can make a tiny difference if
we want, and actually in this case we probably do want, to look for lines that
only start with From at the beginning. And so in Python, with just strings
what we would do is we'd say, oh, no, we're not going to use the find,
we're going to use the startswith method. So line is that string of characters
that come in from the line, and if it starts with a 'From:',
that's the line we want to send. That's the line we want to print. Well, in regular expressions
we're still going to use search, but we're now going to use one of
these special characters. The ^ is that special character that
matches the beginning of a line. So if you look at ^From, what we're really saying in this little
programming language is, not look for the string ^ but instead say look for
From at the beginning of the line. So we're asking a question here. Dear regular expression,
look through this line and find out if you find the string
From at the beginning. And then return us a true or a false
depending on whether that happens. And so we sort of see,
we change things and you'll see in a bit that we will
slowly but surely add more and more functionality and intelligence
to these little regular expressions, rather than extending and adding more and
more lines of code to our Python code. So that's the caret is the first
thing that we learned. The next thing that we're
going to learn is the dot. The dot is the wildcard character, and that's a little different than
some things like *.* or whatever. Usually the star is a wild card,
but star's not a wild card. The wild card is the dot character. Star means any number of times. Asterisk, any number of times. Okay, so star means any number of times. So if we carefully look
at what this is saying. What this says is, I am interested in,
in my little programing language, I'm interested in matching the start
of the line, followed by the letter X. The letter X is just the letter X,
it's not special. And then followed by any character,
and then these two things go together, star, dot. means any character as many times as
you like, followed by another colon. So the X and the colon are not special,
they're not code. They're not on my little sheet of
things that have meaning but the caret means beginning of line, dot means
any character, and star means as needed. Now, that means that some lines will.
a number of these lines will match. This has an X,
all these lines have X at the beginning. And then they have some number of wildcard
characters, and then they have a colon. So that's all we're saying. Find a line with X at the beginning, some
number of characters, and then a colon. This is okay. If we wanted to say that
was the last character, I could have put a dollar sign here and then
that would say the line would have to end there, but
then none of those things would match. But, so X and colon are real characters
and caret, dot, and asterisk are like part of the little programming language
that we've got going on here. Now, you might want to do
things like not have extra spaces. Right? And so if we look in this previous one. We look at this X-Plane
is behind schedule, well it starts with an X and
then has a colon and has all these things. And we didn't really want to match that but
it looked to the regular expression like a thing that had an X plus a bunch
of characters and then a colon. And if we want to clean that up, we can clean that up by just tuning
our regular expression a little bit. We can be a little more explicit.
We can say, you know what? We don't just want an X at the beginning
of the line, we want an X-. So that says I want to match X-. And then, instead of any character, which a dot would be any character,
but \S. \S on the sheet. \S says a \S says non-blank character, any non-blank character, not white space. And then, instead of star here, star is
zero or more times and plus is one or more times, followed by a colon. So, this little programming language
says the line must start with X-. At least one non-blank character, but they
all have to be non-blank up to the colon. And so, this starts with X-, at least
one non-blank character, and a colon. Starts with X-, any number of non-blank
characters, followed by a colon. This one starts with X-, it's fine. But now it says oops,
I am only looking for things. I gotta find the colon before I find,
I mean, I hit a blank before I found the colon,
so this line doesn't match. So take a look at it. You see how we're just slowly but
surely adding a single character or a couple of characters and
it turns into code, right? These are, we are communicating with this regular expression code and
telling it what we want to do. Now, maybe we are not making any sense. And it's quite often when you're
making a regular expression that you won't make any sense. So, the search tells you the true
or false, whether or not you're going to find what you're looking for or
not find what you're looking for. And then the findall will
go through a string and look for
something to happen multiple times. And so, this slide also introduces a new
syntax and that is the bracket syntax. And, so,
the square brackets enclose a set. And so, the square bracket is
a single character and what's in between the square brackets is the
legal things that we're willing to take. So, what I'm saying here
is this is one digit. So [0-9] that says, one digit. And then the plus is a suffix
that says one or more digits. If you had star it would be zero or
more digits. But in this case,
I'm saying at least one digit. And then any number of digits. Okay? So that is a digit somewhere in
the range zero through nine. You can have things like A-Z in there. The sheet talks about this. So that's a expression. So at least one or more digits. And findall is another method within
the regular expression library that says, go through this string I'm looking at and
find all the situations that all the substrings of that string that
would match my regular expression. So just to express roughly
what we're doing here, here is the string we're looking for. And we're saying, dear regular expression. Find me any string that's one or
more digits and give them back to me. That's one or more digits, that's one or
more digits, and that's one or more digits. It's like give them back to me. And so it pulls those out and it puts them
in a list and so this is the list we get. And there's a lot of code in there. Right?
And this is a relatively simple regular expression. I'm looking for numbers. One digit, two digits,
five digits, 100 digits, doesn't matter, some numbers,
pull them out and give me a Python list. So this is a Python list. So
whenever you're extracting using findall, what you get back is a Python list, and
in that Python list we have three strings, 2, 19, and 42, and
those are exactly those things. So that's a pretty powerful little bit of
code to say go through and find me all the numbers, and give me the numbers,
don't just tell me that they exist, true or false, and
I want the numbers to be pulled out. If you think about how
you'd have to do that. I mean you'd have to use split and you'd have
to do some checking, check for integers. I mean you could do the same thing
with splits and a for loop and a couple of other things but
that's, you know, five, six, eight lines of code that regular
expression does for you. Okay? So findall our way of extracting data. search is our way of asking whether
there is a possible match. findall both says is there a match,
how many matches are there, and what are the things that I extracted? And so, if f you look at this, the same
string, right here, the same string. And I use my one or
more digits extraction, I get three things in a Python list. And if I say, I'm looking for
any number of uppercase vowels, A, E, I, O or U, in a bracket,
remember, that's one character. And this is one or more. One or more uppercase vowels. So find me in this string right here,
find me all of the sequences of one or more uppercase vowels and
give them back to me. So it looks through them, umm, nope. So it gives us back an empty list. So the fact that it didn't find it at
all is why we get back an empty list. We call findall, and
you don't find anything. So it doesn't return false. It always returns a list and
if there was nothing found, it returns a list with nothing in it. Okay? Now these matching operations
are what are called "greedy". And greedy means that as they're
extracting, they're pushing outwards. And it wants to return in the return
codes that you're getting. It wants to return
the largest possible string. So let's just take a look at this. So here is a thing that says, beginning of line, starting with an F,
any character, one or more times. Right? Actually I already had
those arrows right there. First character has got to be an F
in the first character of the line, followed by any number of characters, blank. nonblank, because dot matches
all characters, and then colon. So you think, oh that's going to
go from colon and give me that. But it doesn't, because it's greedy. And so it could either give you
this matches and that matches. We could tell it I'm not
interested in a colon. You could say start with an F, as many non-colon characters stopping
with a colon, you could say that. So it could either legitimately say that
is starting with F and ending in colon. Or, it legitimately could say,
starting with F and ending in colon. Greedy says, prefer the larger. Right?
So, greedy says make the string as large as possible. And so that's why we get this string. Both strings legitimately match, but if you have a choice,
take the largest of the two strings. So, non-greedy matching. Sometimes if you really want to only stop
at that first colon, then you add this little question mark, and the question
mark says, don't do the greedy matching. Okay? And so that says satisfy
the regular expression with the shortest string rather
than the longest string. And so other than this greedy matching,
it starts with an F and ends in a colon. Starts with an F and ends in a colon. Well, non-greedy matching
says prefer this one. That's the one we want because
don't go as far as you can and sometimes that's really important. And so in this one we get 'From:' back. Okay? So, that gives us kind of a start. We'll continue in a moment
on regular expressions.