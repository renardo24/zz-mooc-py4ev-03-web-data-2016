So now we're actually going to
write our real web crawler, and we're going to actually parse the HTML. And most of the time I show
you the hard way first, and then I show you the easy way second. But in this one we're not
gong to bother with that. I'm like oh, look how much easier it is. No, we're just going to
start with the easy way. The problem is if you look at HTML,
just here's some sample HTML, you can break this stuff on,
it doesn't have to all be on one line. Well, you might write HTML, makes really
good sense, and so it could be ugly. And you don't even want to
know the rules of HTML. It's bad enough to know the rules of
HTML when you're writing web pages, let alone trying to read
someone else's crazy HTML. And as HTML starts coming
from applications, if you're going to learn how to do
web development like in PHP, when HTML that comes out of these programs
it's sometimes really ugly with blanks and newlines and all kinds of crap. And it's all legit, right? And the browsers are really
smart about compensating for ugly HTML that's valid but ugly HTML. And slowly but surely you could
write a parser for HTML. But then you would find out like,
oh, somebody did this page, and they used a single quote or
they forgot the quotes, or whatever. Well someone's done that already, and they
wrote this thing called Beautiful Soup. Beautiful Soup is sort of a play on the
children's book called Stone Soup where you kind of throw a bunch of junky
things in and it turns out great. Well, I think that's what they mean
when they call it Beautiful Soup, that HTML's all junky. And if you throw all the crappy
HTML into Beautiful Soup, then what comes out of Beautiful Soup is
wonderful and delicious parsable HTML. So this is also a good time to talk
about Python 2 versus Python 3. This class of course for
now is in Python 2, and so what I'm going to show you is how
to use Beautiful Soup in Python 2. The concepts are very similar. The installation's a little different for
Python 3, and Beautiful Soup is ported both
to Python 2 and Python 3. So you can use this library. It's a great library. Now if you're doing Python 2,
which is our Python, you can download the file BeautifulSoup.py, put it
in the same folder as your Python code. There are alternative ways to put this in. But this is the crude way. And if you're having trouble finding
BeautifulSoup.py, it's on my Python for Informatics website. And you can download it from there, either
that or from the original crummy.com. So here is a easy way to do it. I mean,
literally this is the whole program. What we're going to do in this program
is we're going to retrieve a web page, and we are going to parse the web page, and we're going to look at all the anchor
tags and print out the hrefs. That's it, this is the whole thing,
thanks to Beautiful Soup. We're not doing any regular expressions,
any find operations, nothing, because Beautiful Soup does it all. So if we look at this,
we have imports at the top. We import the urllib. So that's how we're going to
actually read the HTML data and retrieve it into our program. Then we're going to import the library, all the routines that are in
the BeautifulSoup.py file. That's kind of what that says. We're going to use raw_input. That's familiar to us,
how we prompt for the name of the URL. Then we're going to call urllib.urlopen
with the URL as parameter. Now the one thing we haven't done before is
we're just going to call the read method on that, and what that means says
read it all, newlines and all. We've done this before. And so it gives us all the lines in a single
call with the newlines intact. But if you think about it, well, in this
case, we're not going to do any splitting, but it's okay to read it all. The pages shouldn't be all that long,
and so this not only opens the URL but reads it. So we've collapsed that down
to a single line, read it all. So what we get, this is a string. I call it html, but it could be anything. html is a string, which is the entire
web page with less thans and greater thans and newlines. That's what that line does. Again with Python, collapses right
down to one line, really nice. So Beautiful Soup. We're going to call
Beautiful Soup, say munge this, read this, figure this out. Here's our string that we read,
and then make sense of it and give us back this soup object. So soup is neither a string nor a boolean
or a dictionary, it's lots of stuff. It is the parsed HTML data, and
then you can ask soup questions, okay? So this is a soup object that's there for
you to make use of. And so we can retrieve a list of tags
by saying soup and pass the a tag. And what that really is looking for is things that look like
a dot dot dot slash, slash a. So that's what an anchor tag looks like. And what we're saying is,
find me all the tags. So don't find me p tags. Don't find me bold tags. Don't find me any of that stuff,
just give me the tags. And what we're really getting is this
data right here that's the tag itself. Okay? And so that is a list of tags. And so if this had one anchor tag,
this would be a list of one tag. If there were zero anchor tags,
we'd get no tags. This is a really smart lookup that
reads the entire HTML document and however many tags are there,
it gives them to us. Okay? Ignores all these other tags and
just finds those tags. So that is a list of tags. It's kind of like a dictionary. Because if you look at what the tag says, it's like a href = quote,
[NOISE], double quote, [NOISE]. And so,
these things here are called attributes. And so when we grab the tag,
we get all of this, and then it parses these things as well, hrefs
as key-value pairs in like a dictionary. And so what we can do is we can look
at all the tags in the document. For tag in tags, that's looping through
all the anchor tags in the document. How many a's there are in this document,
da, da, da, da, da, loop through them all, ignoring everything else in the document. And then give us the href value for
each of those tags. And we're going to use a get, and
we're going to have a default value of None. So that we can, if there is no href, because it's possible to do
an anchor tag with no href. And that, literally, is going to retrieve the
document, and show us all the anchor tags. Seriously, that's it, that's it. So let's just run that. Come back. What's the name of that file again? urllinks.py, let's take a look at it. That's it. Read a web page, any web page we type,
read it, use Beautiful Soup to parse it. Look for all the anchor tags,
loop through the anchor tags, and print out the hrefs, okay? python urllinks.py. So let's just start with that other file, http://www.dr-chuck.com/page1.htm. And we know what this looks like. And you could put print statements to
print the HTML out, but I don't have that. It's going to retrieve it with a .read, pull
all the text, pass it into Beautiful Soup. And then loop through and print out
all the hrefs of all the anchor tags. End of story, right? Did it. That's just seven lines of code. And you just did all that work, because
we're depending on the urllib library and the Beautiful Soup library. But we don't have to just
have a page with one link. We could say, http://www.dr-chuck.com. See what happens there. See how many links I've got on that page. So there we go.
That is all the links on that page, right? It read all that stuff. So right here, dr-chuck.com, these from
here down are all the links in that page. Now you can see how easy it is to
eventually write a web scraper, which we will do later in
the book basically. Okay? And so that's it. That, and [LAUGH] I guess that's it. That's the simple essence of using
[LAUGH] really powerful Python libraries both built in with urllib and
with Beautiful Soup to both retrieve and parse these things in just
a couple lines of code. And I'm going to guess that the first
Google crawler was probably looked just about like this. These days it's super sophisticated code. But in the early days you could write
a crawler that used this essential bit of code that would retrieve these pages,
parse them using Beautiful Soup or a similar library. And then make a list of the next
things that have to be parsed and then just go and go and go and go. And eventually you sort of end up
with a full list of everything, and you too could have a search engine. But it's a little late now, because
someone already did a search engine. So if we take a look
at this whole chapter, the Internet and the Internet
protocols are very beautiful and very complex and knit us all together and
connect applications to applications. In a programming sense, the lowest that
we tend to talk to is the sockets, which are like,
make a phone call to this address, which is like make a phone call through
a phone number with an extension. The ports are the extensions that
allows us to talk to the web server, the mail server, the login server. So on one host you can
have multiple servers. And then once you talk to that server, you've got to know
the application protocol. And in this case we played with
the HTTP application protocol. We send these GET requests. And we've talked to the socket library. We've talked to the urllib library. And then we talked using
the Beautiful Soup library. And, when it's all said and done,
our programs end up relatively small. And Python has really good support for
talking across the network to these various applications, retrieving,
parsing, and making sense of it. Now, up next, we'll be talking to
things that are even more intelligent. Now HTML is not designed to transfer data. But using Beautiful Soup,
we can kind of pretend that it is data, and tear our stuff out
that we want to look at. But, up next we're going to talk to things
that actually want to talk with us, and give us data in an even better format
than HTML that makes it easier and more natural for
us to make sense of that data. So that's what we're going to do next. We're going to keep using
the network to retrieve data, but we're going to use very different
techniques in the next lecture.