So now that we understand what
a service oriented architecture is, we know how to write Python to read and
write this stuff, and we can talk URLs, we're going to bring it all together now, and we're going to actually talk to
some real live web services. As we talk to web services,
we have to understand how they think. And we may later be building
applications that export web services. But the first thing we're going to do
is use somebody else's web service. And we'll have to read
their documentation. And the documentation, they often
call these web services APIs, or application program interfaces, because they have an application that's got brains
and skill and all this stuff, and we have an interface that we can make use of
the services that an application provides. So you're like, oh, what's the API
to Twitter, or what's the API for this, or what's the API for that? When you hear this word API,
that's really what we're talking about. It is a defined set of rules to
interface with an application program. There's a couple different
web service technologies. Thankfully, the crappy old
SOAP is really complex. If you work at a big corporation,
they might still use crappy SOAP. It's whatever, it's just whatever. Just tell them they should do REST, and if they insist on you using SOAP,
then you should double your salary. Just tell them that. Say SOAP is so bad, that you have to
pay me twice as much to work on it. REST is the way we're going to do this. I'll have a video about REST from
the creator of REST, Roy Fielding. And you can see this. It's a way of sort of viewing the services
as a set of resources that we're pulling. And it makes this interaction be more like the simple use of the web
the way browsers use it. We go get data. It's in a different format, but we're still
sort of saying give me this thing, give me this thing, give me this thing, and
that data happens to be an XML or a JSON. So REST is a pattern that we'll use. So the first API that we're going to play
with is the Geocoding API from Google. Now Google has a great search engine,
they have all this data about the shape of the world, and maps, and
all this stuff, and you can make use of it. If you have, say, a survey and
you ask people where they live, well they're going to be rather imprecise. They might say Ann Arbor, Michigan, or they might say eastern New Jersey,
or they might say Beijing, China. And you may not, it may be very difficult, but you can actually send all those
queries to Google's Geocoding API and you will get a ton of data based
on Google's best estimate. It's the same thing as what you would type
in Google Maps, like Ann Arbor, Michigan. It figures out what you mean. Well, this figures out what you mean, and gives you back a bunch of data
about the statement that you make. So here's a little example,
it's much longer than this, but this is a JSON interface. And if you know what the API is, and
you've got to read all the documentation, so go ahead and
read all this documentation. And the documentation will tell you,
unless they've changed it, which they have the right to change it,
it's free, so we can't complain too much. But for now, what that says is, you say, maps.googleapis.com/maps/api/geocode/json? All of this is necessary. Sensor=false&address=, and
then you put the actual address. Ann Arbor Michigan. Plus means space, plus means space. And %2C, I think means comma. Okay? So it turns out that what's cool
about these REST-based web services, is I can actually just take that URL, this one right here, copy it,
and go stick it into a browser. Come on, browser. And I'm going to just paste that URL,
exactly as it says. Now, again, you derive this from the documentation to figure out how
you're supposed to put this URL together. But in effect, this part here,
this part right here, that was that user-entered data. Right?
And you encode it a certain way, and there's actually libraries in
Python that help you do this, but for now I just want to show you
how you can type this in. Ann Arbor, Michigan, and
when I hit enter, it retrieved JSON. This is like a web page,
but it's a resource. It's actually a dynamic resource,
because the resource isn't the same. It depends on the query values that
we put in here, Ann Arbor, Michigan. And so it tells us something about this. I'll make it big. It's in Michigan. It's the United States. Here is the geometry, the boundary of it. It is the center of it. Here's the center of it. Latitude and
Longitude are the center of it. That would allow you to
place this on a map, if you want to take all your students and
put it on a map. So if you can retrieve this
JSON based on this parameter, then you can parse this JSON using
the JSON library that we just showed you. Okay, so that's what we're going to do. And the real application for
this might be a survey. As a matter of fact,
it's the application I use it for, when we do survey data of all
the students and we want to geolocate them. I just take the words that you put into
a field and I send it to this API. We’ll talk about some of the issues and
limitations, but let’s just take a look at how
it actually works to do this. So this is a bit longer bit of code but let’s take a look at
what it’s really doing. It’s not all that complex. When we run it,
our program's going to ask for a location, and we'll type one in,
Ann Arbor, Michigan, it can take anything. And so I am going to use urllib, which is how we retrieve data on
the Internet, we need a URL, and import JSON. And JSON is how we are going
to use the parse the data that comes back. So the URL that we're going to use is this
URL that we read from the documentation. We're going to like stick the question
mark at the end of it. And then we're going to write a loop that
goes around, and around, and around, and asks for the location. And checks to see if the input
that came back is zero, and if it is we're going to end our loop. And, then we're going to take
that service URL, and remember the part where we had the plus,
Ann Arbor, and the plus, and the %2C, well,
there's a magical thing inside of Python. We don't need to know that,
we call urllib.urlencode, and we have the key-value pairs
in a little tiny dictionary. That's what this curly brace is,
it's a dictionary of key-value pairs. Sensor equals false and address equals this address right here
is whatever address we typed in and it does all that encoding for
us that is what it is saying. Encode this address value that the user
typed in a way that's legal to be put on to a URL. Do that for
me please automatically. Right? This is string concatenation, and
then it will print this whole URL out. And then what does it do? It does a urlopen of the URL, and
then it does a read of that whole thing, which reads that JSON, which is
the curly braces and all that stuff. Right? So that reads the JSON,
so this line right here. In Python,
we don't have to do too much work. Open URL, read the whole thing,
newlines and all. And we are going to get something
that looks like that. This is what's going to be in that string. So that's what's in the string,
and we're going to use a try and except because sometimes if this data is
bad then loads will blow up and we're going to do an except, I mean we're going to do
a try and except in case that blows up. And then we're going to take a look at the JavaScript to see if
we got an OK status. So js ends up being the parsed JSON.
Right? So js is the whole JSON. This is a little bit tricky,
but ultimately, we're just reading inside
here to check the status. Oh it's not down here. Oh, no, status is right there. So we're checking is the status OK. We're also being a little more clever in
the code that I wrote to check to see if it went wrong. If the exception happened, or
if the exception happened or the status is not OK,
then I just print out the nasty stuff and then I continue back up and loop again. Then, the first thing I do is, json, this is the JSON library, dumps,
says dump in a string, this is, and takes the JSON, which is the parsed,
in this case it is a parsed dictionary, because the outer thing is a
bracket, which means this is a dictionary. There are lists within it, but
the outer thing is a dictionary. And so this is a dictionary,
the parsed dictionary, and then this is called pretty printing,
and I mentioned that before. And it says take this stuff, print
it out nicely, with an indent of four. So it does all the indenting and
all that stuff so it's readable by humans. The computers really don't
care if it's indented but we humans care a lot if it's indented. And then what we're going to do,
is we're going to parse it. Now, it's a little tricky. And when I'm writing this code right here,
I don't all write it at once. But what you're really doing is
you're looking at the JSON syntax and you're sort of diving down into the JSON. And I'd sort of do it one at a time, and I'd use
a lot of print statements, do it over and over and over again. But then when it's all pretty and I know exactly where the thing is that
I'm looking at, I'm in good shape. So what we're doing is
we're taking the results. So let's go back. The results is this. And notice that the results is a list. And it turns out that there
is more than one location. So this is a list of locations, but what
I'm doing is just taking the first one, so results sub zero,
all right, results sub zero. And then if we go up,
results sub zero is this whole object. And then we have geometry,
which is this little object. And then within geometry,
we go to location. So now we're down into this little thing. And then we pull out the latitude. So this, let me clear that. Let me clear that to make it clearer. Clear. This whole thing, that expression extracts this number. So, that's how you sort of
build your way down in. And we had to go through
a dictionary that included a list, that had a list of dictionaries and then
a dictionary that had a dictionary and another dictionary inside of it. Right? But that's what we're doing here. Look up in a dictionary, then the first
element of the list that you get, then there's a dictionary
inside of a dictionary and then a value inside the dictionary,
and out comes the latitude. It takes a while to write this, right? I mean, it's much easier for
me to explain this than to write this, and if you watched me write this, you would
see I don't write it all correct, because it'll usually blow up
with a error of some type. I write it one piece. I kind of do it here. I get that part working, and then I add
another thing and then I add another thing. And finally I get the thing I want, so you don't try to write
this all at the same time. So this pulls out the latitude and
the longitude, and I print it out. And then I just pull out the formatted
address which is within results, the first entry and
then the formatted address, which is Ann Arbor, Michigan, USA. So we typed in Ann Arbor,
this is what we typed in, and then Google did the rest of that for
us and gave us back the actual address. This is an address that is sort of like
a more precise address than what we typed. And so it got you know,
pretty, pretty awesome. Okay? Okay, so
it'll take you a while to look at that. That's a pretty complex bit of code. But it's constructing a URL using
this URL code, hitting the URL. Then parsing the JSON and extracting
just the information that we want. So it's a lot of fun actually. A lot of fun. So, let's actually run that guy. It's called geojson.py. So, cat geojson, so there it is. That's that same code I just showed you. So, now I'm going to run it. Python geojson .py. And I'll do Ann Arbor, MI. >> And so you'll see, all right,
that was really fast. So this is the URL that it hit,
we used the encoder to get this little bit right here right, because there are
rules about passing parameters on URLs, we don't need to know the rules,
we just use the encoder. We got 1736 characters back, the status
was OK, this was the geometry. We got the, it also knows this in terms
of like the boundaries of the location, like a square. The actual address, and all these things. The formatted address,
we pulled all that stuff out, and so this is the JSON we got back, and
this is the stuff we extracted, right? So I could say things like north quad,
I don't even know where north quad is. north quad is my office,
will it find it in the right place or not? I don't know. There might be many north quads. It's not. It doesn't know that
north quad is my office. Apparently it thinks that north
quad is in Claremont, California. What are they thinking? I don't know. Oxford, where's Oxford? So there’s Oxford,
it knows where Oxford is obviously. So we can ask where Universal Studios is. Where’s Universal Studios? Florida. Let’s see if I can make
a typographical error. D, I, N, S, E, Y Land,
Disneyland, but I’ve got it wrong. Will is find Disneyland? It can even fix spelling errors. So what you've got is like 35 lines
of Python, or 30 lines of Python code. And you're taking advantage of like all of Google's awesomeness by talking
to the Google API to do this, okay? So I'll just hit enter, and we're done. Okay?
So this is, again. This is how your application
can become more powerful by integrating the services that
are provided by another application, or in the case of you cleaning up data,
it does that too. So, you got to be careful.
There's no such thing as a free lunch. We love Google and
they give us this free geocoding thing. But the data is super valuable, and there are people who would do nothing
except sort of make a really dumb web site to help people look up addresses and
just use all the Google stuff and their web site would be making money and
putting up crappy ads and that stuff. So these folks often
will demand an API key, or charge for usage,
or have rate limiting. And they might change the rules as
things get more and less popular. So that's how we're going to talk to this
first API, but we're not done yet, and there's actually some more issues having
to do with APIs that we'll cover in the next lecture.