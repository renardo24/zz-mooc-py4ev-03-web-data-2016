import urllib
import xml.etree.ElementTree as ET

url = raw_input("Enter location: ")
if len(url) < 1 : exit(1)
print "Retrieving", url

uh = urllib.urlopen(url)
data = uh.read()
print "Retrieved",len(data),"characters"

tree = ET.fromstring(data)
counts = tree.findall('.//count')
print "Count:",len(counts)

total = sum(int(x.text) for x in counts)
print "Sum:",total

