import json
import urllib

url = raw_input("Enter location: ")
if len(url) < 1 : exit(1)

print "Retrieving",url
uh = urllib.urlopen(url)
data = uh.read()
print "Received",len(data),"characters"

js = json.loads(data)
print "Count: ", len(js['comments'])

lst = list()
for item in js['comments']:
    lst.append(int(item['count']))
print "Sum: ", sum([x for x in lst])

