# Note - this code must run in Python 2.x and you must download
# http://www.pythonlearn.com/code/BeautifulSoup.py
# Into the same folder as this program

import urllib
from BeautifulSoup import *

url = raw_input('Enter - ')
repeat_count = raw_input('Enter repeat count: ')
position = raw_input('Enter position: ')

html = urllib.urlopen(url).read()
soup = BeautifulSoup(html)

print 'Retrieving %s' % url

# navigating the links
count = int(repeat_count)
pos = int(position) - 1
while count > 0:
    html = urllib.urlopen(url).read()
    soup = BeautifulSoup(html)
    tags = soup('a')
    url = tags[pos].get('href', None)
    print 'Retrieving %s' % url
    count -= 1

import re
print re.findall('.*known_by_(.*).html$', url)[0]

