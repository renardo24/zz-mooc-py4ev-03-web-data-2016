# Exploring the HyperText Transport Protocol
# 
# You are to retrieve the following document using the HTTP protocol in a way that you can examine the HTTP Response headers.
# 
# http://www.pythonlearn.com/code/intro-short.txt
# There are three ways that you might retrieve this web page and look at the response headers:
# 
# Preferred: Modify the socket1.py program to retrieve the above URL and print out the headers and data.
# Open the URL in a web browser with a developer console or FireBug and manually examine the headers that are returned.
# Use the telnet program as shown in lecture to retrieve the headers and content.
# Enter the header values in each of the fields below and press "Submit".
# 
# Content-Type:
# 
#  
# Content-Length:
# 
#  
# Last-Modified:
# 
#  
# ETag:
# 
#  
# Cache-Control:
#
# -----
# Generated output:
# HTTP/1.1 200 OK
# Content-Type: text/plain
# Content-Length: 467
# Connection: close
# Date: Wed, 13 Jul 2016 20:35:28 GMT
# Server: Apache
# Last-Modified: Mon, 12 Oct 2015 14:55:29 GMT
# ETag: "20f7401b-1d3-521e9853a392b"
# Accept-Ranges: bytes
# Cache-Control: max-age=604800, public
# Access-Control-Allow-Origin: *
# Access-Control-Allow-Headers: origin, x-requested-with, content-type
# Access-Control-Allow-Methods: GET
# 
# Why should you learn to write programs?
# 
# Writing programs (or programming) is a very creative 
# 
# and rewarding activity.  You can write programs for 
# many reasons, ranging from making your living to solving
# a difficult data analysis problem to having fun to helping
# someone else solve a problem.  This book assumes that 
# everyone needs to know how to program, and that once 
# you know how to program you will figure out what you want 
# to do with your newfound skills.  
# 

import socket


mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mysock.connect(('www.py4inf.com', 80))
mysock.send('GET http://www.pythonlearn.com/code/intro-short.txt HTTP/1.0\n\n')

while True:
    data = mysock.recv(512)
    if ( len(data) < 1 ) :
        break
    print data;

mysock.close()
